﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SpiBotback.Database;
using SpiBotback.Providers;
using SpiBotback.Services;
using SpiBotback.Services.Configs;

namespace SpiBotback
{
    public class Startup
    {
        private IHostingEnvironment CurrentEnvironment { get; set; }

        public Startup(IHostingEnvironment env)
        {

            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();

            CurrentEnvironment = env;

            Configuration = builder.Build();


        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Add functionality to inject IOptions<T>
            services.AddOptions();

            // Add our Config object so it can be injected
            services.Configure<WatsonAssistantConfig>(Configuration.GetSection("ibm:watsonAssistant"));
            services.Configure<DiscoveryConfig>(Configuration.GetSection("ibm:discovery"));
            services.Configure<BDDConfig>(Configuration.GetSection("BDD"));


            //Add cords handler
            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin",
                builder => builder.WithOrigins("*")
                .AllowAnyHeader()
                .AllowAnyOrigin()
                .AllowAnyMethod());
            });

            //Add cors polict to all controller 
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowMyOrigin"));
            });



            var configBDD = Configuration.GetSection("BDD").Get<BDDConfig>();
            var connectString = $"Server=localhost\\SQLEXPRESS;Database=supinfobotdb;Trusted_Connection=True;";

            // Use SQL Database if in Azure, otherwise, use SQLite
            if (!CurrentEnvironment.IsDevelopment())
                services.AddDbContext<SpibotDBContext>(options =>
                        options.UseSqlServer(connectString, provider =>
                        {
                            provider.EnableRetryOnFailure();
                        }
                      ));
            else
                services.AddDbContext<SpibotDBContext>(options =>
                        options.UseSqlite("Data Source=localdatabase.db"));

            services.BuildServiceProvider().GetService<SpibotDBContext>().Database.Migrate();



            services.AddSingleton<IbmWatsonService>();
            services.AddSingleton<IWatsonAssistantProvider, WatsonAssistantProvider>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IBotService, BotService>();
            services.AddSingleton<IIbmWatsonService, IbmWatsonService>();
            services.AddSingleton<IDiscoveryProvider,DiscoveryProvider>();
            services.AddSingleton<ISaveService, SaveService>();
            services.AddSingleton<LogService>();


            services.BuildServiceProvider().GetService<LogService>().registrerlog();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddDebug();
            loggerFactory.AddConsole();
            loggerFactory.AddEventSourceLogger();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/api/Error");
                app.UseHsts();
            }

            //Active cors policy
            app.UseCors("AllowMyOrigin");

            app.UseHttpsRedirection();
            app.UseMvc();

 
        }
    }
}
