﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SpiBotback.Models;
using SpiBotback.Providers;
using SpiBotback.Services;
using SpiBotback.Services.Configs;
using SpiBotback.Services.Security;

namespace SpiBot_back.Controllers
{
    [Route("")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        ISaveService saveService;

        public HomeController(ISaveService saveService)
        {
            this.saveService = saveService;
        }

        [HttpGet]
        [Throttle(Name = "ThrottleTest", Seconds = 1)]
        public ActionResult<string> Get()
        {
            LogService.writeLog("Someone goes on api base path");
            return "Welcome to spibot api";
        }
    }
}
