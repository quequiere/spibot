﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SpiBotback.Database;
using SpiBotback.Models;
using SpiBotback.Providers;
using SpiBotback.Services;
using SpiBotback.Services.Configs;
using SpiBotback.Services.Security;

namespace SpiBot_back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IBotService botService;

        ISaveService saveService;

        public ChatController(IUserService userService, IBotService botService, ISaveService saveService)
        {
            this.userService = userService;
            this.botService = botService;
            this.saveService = saveService;
        }

        // POST api/values
        [HttpPost]
        [Throttle(Name = "ThrottleTest", Seconds = 1)]
        public ActionResult<UserFrontResponse> Post([FromBody] UserFrontRequest userRequest)
        {
            try
            {
                User u = this.userService.GetUserById(userRequest.userID);
                u.lastMessage = userRequest.message;

                var botResponse = botService.getBotResponse(u);

                Task.Factory.StartNew(() =>
                {
                    if (!string.IsNullOrEmpty(u.lastMessage))
                    {
                        saveService.addConversation(u.lastMessage, SenderType.user, u);
                        saveService.addConversation(botResponse.text, SenderType.bot, u);
                    }
                  

                });


                return botResponse;
            }
            catch(Exception e)
            {
                LogService.writeLog("Error was throw while responding to user");
                LogService.writeLog(e.Message);
                LogService.writeLog(e.StackTrace);
                throw e;
            }


        }

 
    }
}
