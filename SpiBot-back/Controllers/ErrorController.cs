﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpiBotback.Models;

namespace SpiBotback.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ErrorController : Controller
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            var e = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (e != null && e.Error !=null)
            {
                HttpCustomException custom = null;
                if (e.Error is HttpCustomException)
                {
                    custom = e.Error as HttpCustomException;
                }
                else
                {
                    custom = new HttpCustomException(StatusCodes.Status500InternalServerError, e.Error, e.Path);
                }

                var error = Json(custom);
                error.StatusCode = custom.statutCode;

                return error;
            }

            return Json("No error handled");

        }

    
    }
}