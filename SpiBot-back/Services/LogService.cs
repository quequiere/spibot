﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Services
{
    public class LogService
    {
        private static ILogger<LogService> log;

        readonly ILogger<LogService> _log;

        public LogService(ILogger<LogService> log)
        {
            _log = log;
        }

        public void registrerlog()
        {
            log = this._log;
        }

        public static void writeLog(string s)
        {
            log.LogInformation(s);
        }
    }
}
