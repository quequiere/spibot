﻿using SpiBotback.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Services
{
    public class UserService : IUserService
    {
        private readonly IIbmWatsonService ibmWatsonService;

        private Dictionary<string, User> users = new Dictionary<string, User>();

        public UserService(IIbmWatsonService ibmWatsonService)
        {
            this.ibmWatsonService = ibmWatsonService;
        }

        public User createUser()
        {
            string generatedID = Guid.NewGuid().ToString();
            User user = new User()
            {
                sessionID = ibmWatsonService.generateSessionID(),
                userID = generatedID
                
            };

            users.Add(generatedID, user);

            return user;
        }

        public User GetUserById(string id)
        {
            if(id!=null && users.TryGetValue(id,out User user))
                return user;
            return createUser();
        }
    }

    public interface IUserService
    {
        User GetUserById(string id);

        User createUser();
    }
}
