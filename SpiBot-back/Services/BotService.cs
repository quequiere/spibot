﻿using IBM.Watson.Assistant.v2.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpiBotback.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Services
{
    public class BotService : IBotService
    {
        private readonly IIbmWatsonService ibmWatsonService;

        public BotService(IIbmWatsonService ibmWatsonService)
        {
            this.ibmWatsonService = ibmWatsonService;
        }

        public UserFrontResponse getBotResponse(User user)
        {
            MessageOutput assistantResponse = ibmWatsonService.getAssistantResponse(user);

            user.userWatsonContext.resultOder = null;
            user.messageToSend.Clear();
            user.buttonsToSend.Clear();

            doWork(user, assistantResponse);

            return new UserFrontResponse()
            {
                text = string.Join("<br>", user.messageToSend) ?? throw new HttpCustomException("No response from bot"),
                userID = user.userID,
                buttons = user.buttonsToSend
            };
        }

        public void doWork(User user, MessageOutput response)
        {
            var finalResponse = response;
            var allResponses = new List<MessageOutput>();

            allResponses.Add(response);


            if (response.UserDefined != null)
            {
                if (response.UserDefined.TryGetValue("watsonOrder", out object o))
                {
                    JObject json = o as JObject;

                    WatsonOrder order = json.ToObject<WatsonOrder>();
                    switch (order.OrderName)
                    {
                        case "discoveryResearch":
                            ibmWatsonService.getDiscoveryResponse(user);
                            getBotResponse(user);
                            break;
                        case "discoveryDisplay":
                            response.Generic.Add(new DialogRuntimeResponseGeneric()
                            {
                                ResponseType = "text",
                                Text = user.localData.discoveryResults
                            });
                            break;
                    }
                }

                if (response.UserDefined.TryGetValue("buttons", out object jsonButtons))
                {
                    var jsonButtonsList = jsonButtons as JArray;
                    jsonButtonsList.ToObject<List<string>>().ForEach(cb => user.buttonsToSend.Add(cb));
                }

            }

            allResponses
                .Where(r => r != null)
                .SelectMany(r => r.Generic)
                .Where(m => m.ResponseType == "text" && !string.IsNullOrEmpty(m.Text))
                .Select(m => m.Text)
                .ToList()
                .ForEach(t => user.messageToSend.Add(t));
        }

        public void getDiscoveryResponse(User user)
        {
            throw new NotImplementedException();
        }
    }

    public interface IBotService
    {
        UserFrontResponse getBotResponse(User user);

        void getDiscoveryResponse(User user);
    }
}
