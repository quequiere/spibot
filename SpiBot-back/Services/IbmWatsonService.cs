﻿using IBM.Cloud.SDK.Core.Util;
using IBM.Watson.Assistant.v2;
using IBM.Watson.Assistant.v2.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SpiBotback.Models;
using SpiBotback.Models.back;
using SpiBotback.Providers;
using SpiBotback.Services.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SpiBotback.Services
{
    public class IbmWatsonService : IIbmWatsonService
    {
 
        private readonly IWatsonAssistantProvider watsonAssistantProvider;
        private readonly IDiscoveryProvider discoveryProvider;

        public IbmWatsonService(IWatsonAssistantProvider watsonAssistantProvider, IDiscoveryProvider discovery)
        {
            this.watsonAssistantProvider = watsonAssistantProvider;
            this.discoveryProvider = discovery;
        }

        public string generateSessionID()
        {
           return watsonAssistantProvider.generateSessionID();
        }

        public MessageOutput getAssistantResponse(User u)
        {
            var callResult = this.watsonAssistantProvider.GetAssistantResponse(u).Result.Output;
            LogService.writeLog("Receive from bot ");
            LogService.writeLog("");
            LogService.writeLog("");
            LogService.writeLog(JsonConvert.SerializeObject(callResult, Formatting.Indented));
            LogService.writeLog("");
            LogService.writeLog("");
            LogService.writeLog("");

            return callResult;
        }

        public void getDiscoveryResponse(User u)
        {
            var discoveryResponse =  this.discoveryProvider.DetailedDiscoveryResponse(u);
            var articlesList = discoveryResponse.Result.Results
                .Select(a => new Article()
                {
                    title = HttpUtility.HtmlEncode(a.Title),
                    url = HttpUtility.HtmlEncode(a.Get("url") as string)
                })
                .Select(a =>$"<a target=\"_blank\" href={a.url}>{a.title}</a>").ToList();

            if (articlesList.Count > 0)
            {
                u.userWatsonContext.resultOder = ResultOrderEnum.success;
                u.localData.discoveryResults = string.Join("<br>", articlesList);
            }
            else
            {
                u.userWatsonContext.resultOder = ResultOrderEnum.fail;
                u.localData.discoveryResults = null;
            }
                

          
        }
    }

    public interface IIbmWatsonService
    {
        MessageOutput getAssistantResponse(User u);

        void getDiscoveryResponse(User u);

        string generateSessionID();
    }


}
