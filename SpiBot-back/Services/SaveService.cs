﻿using SpiBotback.Database;
using SpiBotback.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Services
{
    public class SaveService : ISaveService
    {
        private SpibotDBContext db;

        public SaveService(SpibotDBContext db)
        {
            this.db = db;
        }

        public void addConversation(string message, SenderType type, User user)
        {

            ConversationHistory currentConv = null;

             var possibleConv = this.db.conversationHistories.FirstOrDefault(c => c.userID == user.userID);

            if (possibleConv!=null)
            {
                currentConv = possibleConv;
            }
            else
            {
                var createdEntity = this.db.conversationHistories.Add(new ConversationHistory(user.userID, new List<ConversationMessage>()));
                currentConv = createdEntity.Entity;
            }


            currentConv.messages.Add(new ConversationMessage() {
                message = message,
                sender = type
            });

            this.db.SaveChanges();
        }
    }

    public interface ISaveService
    {
        void addConversation(string message, SenderType type, User user);
    }
}
