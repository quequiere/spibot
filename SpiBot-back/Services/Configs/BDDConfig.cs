﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Services.Configs
{
    public class BDDConfig
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
