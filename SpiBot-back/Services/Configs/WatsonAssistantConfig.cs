﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Services.Configs
{
    public class WatsonAssistantConfig
    {
        public string apiKey { get; set; }
        public string endPoint { get; set; }
        public string versionDate { get; set; }
        public string assistantID { get; set; }
    }
}
