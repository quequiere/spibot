﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Services.Configs
{
    public class DiscoveryConfig
    {
        public string apiKey { get; set; }
        public string endPoint { get; set; }
        public string environmentID { get; set; }
        public string versionDate { get; set; }
        public string collectionId { get; set; }

    }
}
