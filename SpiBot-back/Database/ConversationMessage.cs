﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Database
{
    //[Table("ConversationMessageTable")]
    public class ConversationMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string message { get; set; }
        public SenderType sender { get; set; }
        public virtual ConversationHistory GetConversationHistory { get; set; }

        public ConversationMessage()
        {

        }

        public ConversationMessage(string message, SenderType sender)
        {
            this.message = message;
            this.sender = sender;
        }
    }
}
