﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Database
{
    public class SpibotDBContext : DbContext
    {
        public SpibotDBContext(DbContextOptions<SpibotDBContext> options) : base(options)
        {

        }

        public DbSet<ConversationHistory> conversationHistories { get; set; }

    }
}
