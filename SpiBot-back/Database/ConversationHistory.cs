﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Database
{

    //[Table("ConversationHistoryTable")]
    public class ConversationHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string userID { get; set; } 
        public virtual List<ConversationMessage> messages { get; set; }

        public ConversationHistory(string userID, List<ConversationMessage> messages )
        {
            this.userID = userID;
            this.messages = messages;
        }

        public ConversationHistory()
        {
        }
    }
}
