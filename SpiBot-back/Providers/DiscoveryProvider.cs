﻿using IBM.Cloud.SDK.Core.Http;
using IBM.Cloud.SDK.Core.Util;
using IBM.Watson.Discovery.v1;
using IBM.Watson.Discovery.v1.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SpiBotback.Models;
using SpiBotback.Services;
using SpiBotback.Services.Configs;

namespace SpiBotback.Providers
{
    public class DiscoveryProvider : IDiscoveryProvider
    {

        private readonly IDiscoveryService discovery;
        private readonly IOptions<DiscoveryConfig> configDiscovery;

        public DiscoveryProvider(IOptions<DiscoveryConfig> configDiscovery)
        {
            this.configDiscovery = configDiscovery;

            TokenOptions discoveryAssistantKey = new TokenOptions()
            {
                IamApiKey = configDiscovery.Value.apiKey,
                ServiceUrl = configDiscovery.Value.endPoint,
            };


            discovery = new DiscoveryService(discoveryAssistantKey, configDiscovery.Value.versionDate);
        }

        public DetailedResponse<QueryResponse> DetailedDiscoveryResponse(User user)
        {
            DetailedResponse<QueryResponse> discoveryResponse = discovery.Query(
                environmentId: configDiscovery.Value.environmentID,
                collectionId: configDiscovery.Value.collectionId,
                count: 3,
                naturalLanguageQuery: user.lastMessage,
                passages: false,
                returnFields:"title,url"
                );

            LogService.writeLog("Received from discovery ");
            LogService.writeLog("");
            LogService.writeLog("");
            LogService.writeLog(JsonConvert.SerializeObject(discoveryResponse, Formatting.Indented));
            LogService.writeLog("");
            LogService.writeLog("");
            LogService.writeLog("");


            return discoveryResponse;
        }
    }

   public interface IDiscoveryProvider
    {
        DetailedResponse<QueryResponse> DetailedDiscoveryResponse(User user);
    }
}
