﻿using IBM.Cloud.SDK.Core.Http;
using IBM.Cloud.SDK.Core.Util;
using IBM.Watson.Assistant.v2;
using IBM.Watson.Assistant.v2.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SpiBotback.Models;
using SpiBotback.Models.back;
using SpiBotback.Services;
using SpiBotback.Services.Configs;
using System;


namespace SpiBotback.Providers
{
    public class WatsonAssistantProvider : IWatsonAssistantProvider
    {

        private readonly AssistantService assistant;
        private readonly string assistantID;

        public WatsonAssistantProvider(IOptions<WatsonAssistantConfig> config)
        {
          
            TokenOptions watsonAssistantToken = new TokenOptions()
            {
                IamApiKey = config.Value.apiKey,
                ServiceUrl = config.Value.endPoint,
            };

            assistant = new AssistantService(watsonAssistantToken, config.Value.versionDate);
            assistantID = config.Value.assistantID;

        }



        public DetailedResponse<MessageResponse> GetAssistantResponse(User user)
        {
            if (user.sessionID == null)
                throw new Exception("User session ID is null");

            WatsonRequest resquest = new WatsonRequest()
            {
                assistantID = this.assistantID,
                sessionID = user.sessionID,
                input = new MessageInput()
                {
                    Text = user.lastMessage,
                    Options = new MessageInputOptions() {
                        ReturnContext = true,
                        AlternateIntents = true
                    }
                },
                context = new MessageContext()
                {
                    Skills = user.userWatsonContext.toContext(),
                    Global = new MessageContextGlobal()
                    {
                        System = new MessageContextGlobalSystem() {
                            UserId=user.sessionID
                        }
                    }
                }
            };


            LogService.writeLog("Sended to watson ");
            LogService.writeLog("");
            LogService.writeLog("");
            LogService.writeLog(JsonConvert.SerializeObject(resquest, Formatting.Indented));
            LogService.writeLog("");
            LogService.writeLog("");
            LogService.writeLog("");


            return this.assistant.Message(resquest.assistantID, resquest.sessionID, resquest.input, resquest.context);
        }


        public string generateSessionID()
        {
            try
            {
                return assistant.CreateSession(assistantID).Result.SessionId;
            }
            catch (Exception e)
            {
                throw new HttpCustomException(e, "Failed to create session ID");
            }

        }

        //public DiscoveryResponse GetDiscoveryResponse(DiscoveryRequest request)
        //{
        //    throw new NotImplementedException();
        //}
    }

    public interface IWatsonAssistantProvider
    {
        DetailedResponse<MessageResponse> GetAssistantResponse(User user);

        //DiscoveryResponse GetDiscoveryResponse(DiscoveryRequest request);

        string generateSessionID();
    }
}
