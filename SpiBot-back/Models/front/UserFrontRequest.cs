﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Models
{
    public class UserFrontRequest
    {
        public string message { get; set; }
        public string userID { get; set; }
    }
}
