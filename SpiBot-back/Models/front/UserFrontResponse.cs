﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Models
{
    public class UserFrontResponse
    {
        public string text { get; set; }
        public string userID { get; set; }
        public List<string> buttons { get; set; }
    }
}
