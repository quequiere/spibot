﻿using IBM.Watson.Assistant.v2.Model;
using SpiBotback.Models.back;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Models
{
    public class User
    {
        public string lastMessage { get; set; }

        public List<string> messageToSend { get; set; } = new List<string>();
        public List<string> buttonsToSend { get; set; } = new List<string>();

        public string sessionID { get; set; }

        public string userID { get; set; }

        public BackData localData { get; set; } = new BackData();

        public UserSpibotContext userWatsonContext = new UserSpibotContext();

    }
}
