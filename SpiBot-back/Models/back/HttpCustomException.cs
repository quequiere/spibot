﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Models
{
    public class HttpCustomException : Exception
    {
        public string type { get; set; }

        public int statutCode { get; set; }

        public string path { get; set; }

        public HttpCustomException(int code, Exception e)
        {
            this.statutCode = code;
            this.type = e.GetType().FullName;
        }

        public HttpCustomException(string message) : base(message)
        {
            this.statutCode = 500;
        }

        public HttpCustomException(string message,int errorCode) : base(message)
        {
            this.statutCode = errorCode;
        }

        public HttpCustomException(int code, Exception e, string path)
        {
            this.statutCode = code;
            this.type = e.GetType().FullName;
            this.path = path;
        }

        public HttpCustomException(Exception e, string message) : base(message)
        {
            this.statutCode = StatusCodes.Status500InternalServerError;
            this.type = e.GetType().FullName;
        }

        public string toJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public override string StackTrace => "";

    }
}
