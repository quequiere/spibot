﻿using IBM.Watson.Assistant.v2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Models.back
{
    public class WatsonRequest
    {
        public string assistantID { get; set; }
        public string sessionID { get; set; }
        public MessageInput input { get; set; }

        public MessageContext context {get; set;}
    }
}
