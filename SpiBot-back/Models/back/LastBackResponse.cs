﻿using IBM.Watson.Assistant.v2.Model;
using SpiBotback.Models.back;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiBotback.Models
{
    public class UserSpibotContext
    {
        public ResultOrderEnum? resultOder { get; set; } = null;

        public MessageContextSkills toContext()
        {
            MessageContextSkills global = new MessageContextSkills();
            MessageContextSkill userContext = new MessageContextSkill();
            var userDefined = new Dictionary<string, object>();

            if(resultOder!=null)
            {
                userDefined.Add("result", resultOder.ToString());
                userContext.UserDefined = userDefined;
                global.Add("main skill", userContext);
            }
  
            return global;

        }
     
    }
}
