﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SpiBotfront.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;


namespace SpiBotfront.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public class ConfigController : Controller
    {
        IOptions<BackConfig> backConfig { get; }

        public ConfigController(IOptions<BackConfig> backConfig)
        {
            this.backConfig = backConfig;
        }

        [HttpGet]
        public ActionResult<string> get()
        {
            return backConfig.Value.url;
        }
    }
}
