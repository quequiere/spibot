﻿import React from 'react';
import './ChatComponent.css';
import BotButton from './BotButton';

class HtmlInnerRender extends React.Component {
    render() {
        return (
            <div dangerouslySetInnerHTML={{ __html: this.props.message }} />
        );
    }
}





class BubbleText extends React.Component {

    constructor(props) {
        super(props);
        this.onBubbleClick = this.onBubbleClick.bind(this);
    }

    onBubbleClick() {

        const regex = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;
        let m;

        if ((m = regex.exec(this.props.message)) !== null) {

            var link = m[0];
            console.log("Open link: " + link);
            window.open(link); 
        }
    }

    render() {
        let classNameRender;
        let alignBubble;
        let isBot = false;

        if (this.props.type === "user") {
            classNameRender = "bubble userMessage";
            alignBubble = "userMessageAlign";
        }
        else if (this.props.type === "bot") {
            classNameRender = "bubble botMessage";
            alignBubble = "";
            isBot = true;
        }
        else {
            classNameRender = "error";
            alignBubble = "error";
        }

        var renderText = this.props.message == null ? false : true;

        if (isBot) {
            return (
                <div className={alignBubble}>
                    
                    
                    {
                        renderText &&
                        <div className={classNameRender} onClick={this.onBubbleClick}>
                            <HtmlInnerRender message={this.props.message} />
                        </div>
                    }

                    <div>
                        {
                            this.props.buttons.map
                                (
                                    (i, index) =>
                                        <BotButton key={index} option={i} pressButtonChoice={this.props.pressButtonChoice} />
                                )
                        }
                    </div>

                    
                </div>
            );
        }
        else {
            return (
                <div className={alignBubble}>
                    <div className={classNameRender}>
                        {this.props.message}
                    </div>
                </div>
            );
        }


    }
}

export default BubbleText;