﻿import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';


class ChatBottom extends React.Component {

    constructor(props) {
        super(props);

        this.state = { disabled: true };

        this.addMessage = props.addMessage;

        this.inputField = React.createRef();

        this.handleClick = this.handleClick.bind(this);
        this.handleKey = this.handleKey.bind(this);
    }


    handleClick() {
        if (this.inputField.current.value != null && this.inputField.current.value.length >= 1) {
            this.props.addMessage("user", this.inputField.current.value);
            this.inputField.current.value = "";
            this.setTextBoxState(true);
        }
   
    }

    setTextBoxState(disa) {
        this.setState({ disabled: disa });
    }

    handleKey(event) {
        if (event.key === "Enter")
        {
            this.handleClick();
        }
        
    }

    render() {
        return (
            <div className="inputTextZone">
                <ReactBootstrap.InputGroup className="mb-3">
                    <ReactBootstrap.FormControl disabled={this.state.disabled} ref={this.inputField} aria-describedby="basic-addon1" onKeyUp={this.handleKey} />
                    <ReactBootstrap.InputGroup.Append>
                        <ReactBootstrap.Button onClick={this.handleClick} variant="outline-secondary">Envoyer</ReactBootstrap.Button>
                    </ReactBootstrap.InputGroup.Append>
                </ReactBootstrap.InputGroup>
            </div>
        );
    }
}

export default ChatBottom;