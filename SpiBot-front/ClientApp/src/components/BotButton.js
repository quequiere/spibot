﻿import React from 'react';
import './ChatComponent.css';

class BotButton extends React.Component {

    constructor(props) {
        super(props);
        this.optionClick = this.optionClick.bind(this);
    }

    optionClick(div) {
        this.props.pressButtonChoice(this.props.option,div);
    }


    render() {
        return (
            <div className="buttonBubble shadow-pop-tr buttonBubbleHover" onClick={((e) => this.optionClick(e))}>
                {this.props.option}
            </div>
        );
    }
}


export default BotButton;
