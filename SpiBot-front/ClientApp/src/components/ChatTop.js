﻿import React from 'react';
import logo from '../images/logo.gif';

class ChatTop extends React.Component {
    render() {
        return (
            <div className="topBoard">
                <div className="logo">
                    <img src={logo} alt="logo" />
                </div>
                <p className="title" >SpiBot, le chatbot pour étudiants et professeurs.</p>
                <p>Rechercher un article, voirs vos notes, vous identifier ?</p>
                <p>Facile ! Je peux vous aider.</p>
                <p>Exprimez vous comme vous le souhaitez.</p>
            </div>
        );
    }
}

export default ChatTop;