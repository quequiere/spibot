﻿import React from 'react';
import './ChatComponent.css';
import ChatTop from './ChatTop';
import BubbleText from './BubbleText';
import ChatBottom from './ChatBottom';
import Spinner from 'react-bootstrap/Spinner';
import ReactDOM from "react-dom";

class ChatComponent extends React.Component {

    constructor(props) {
        super(props);

        this.bottomChild = React.createRef();

        this.state = {
            items: [],
            spin: "inherit",
        };

        this.addMessage = this.addMessage.bind(this);
        this.pressButtonChoice = this.pressButtonChoice.bind(this);

        this.userID = null
        this.urlChat = "http://urlunknow.com";;



        fetch('/api/config/')
            .then(r => r.text())
            .then(
                (result) => {
                    this.urlChat = result;
                    this.sendMessageToBack("");
                }

            )

    }

    disableAllButtons() {
        const node = ReactDOM.findDOMNode(this);
        const childs = Array.from(node.getElementsByClassName('buttonBubble'));
        childs.forEach(function (element) {
            element.className = "buttonOff buttonBubble shadow-pop-tr";
            element.onClick = function () { };;
        });
    }

    pressButtonChoice(option, sender) {


        var pressedDiv = sender.currentTarget;


        if (Array.from(pressedDiv.classList).includes("buttonOff")) {
            return;
        }

        if (this.bottomChild.current.state.disabled === false)
        {
            
            this.disableAllButtons();

            this.bottomChild.current.inputField.current.value = option;
            this.bottomChild.current.handleClick();
        }
        
    }

 

 

    sendMessageToBack(textToSend) {

        this.setState(prevState => {
            return {
                spin: "inherit"
            };
        });

        var jsonedMessage = JSON.stringify({
            message: textToSend,
            userID: this.userID,
        })

        console.log("Message sent to back:")
        console.log(jsonedMessage)

        fetch(this.urlChat, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: jsonedMessage
        })
            .then(res => res.json())
            .then(
                (result) => {

                    if (result.statutCode === null || result.statutCode === undefined|| result.statutCode === "200") {
                        var msg = result["text"]
                        var buttons = result["buttons"];

                        if (this.userID == null) {
                            this.userID = result["userID"];
                            console.log("User id has been set to " + this.userID);
                        }

                        this.addMessage("bot", msg, buttons);
                    }
                    else {
                        console.log("Error statut code: " + result.statutCode);
                        console.log("Message: " + result["message"]);

                        if (result.statutCode === 409)
                            this.addMessage("bot", "Ralentissez la cadence, je fait au mieux pour vous aider.")
                        else
                            this.addMessage("bot", "Une erreur est survenue durant votre conversation.")
                    }



                },
                (error) => {
  
                    console.log("Failed to get response from " + this.urlChat);
                    this.addMessage("bot","Désolé, une erreur est apparue lors du traitement de votre demande, veuillez essayer ultérieurement.")
                }
            )
    }

    addMessage(type, content, buttons) {

        this.disableAllButtons();
 
        if (type === "user") {

            this.setState(prevState => {
                return {
                    items: this.state.items.concat({
                        message: content,
                        type: type
                    })
                };
            })

            this.sendMessageToBack(content);
        }
        else
        {
            var messageList = content.split("<br>");

            messageList.forEach((element, index) => {

                setTimeout(
                    function ()
                    {
                        this.setState(prevState => {
                            return {
                                items: this.state.items.concat({
                                    message: element,
                                    type: type,
                                    buttons: []
                                })
                            };
                        });

                        if (buttons != undefined && buttons.length > 0 && (messageList.length-1) === index) {
                            this.setState(prevState => {
                                return {
                                    items: this.state.items.concat({
                                        message: null,
                                        type: type,
                                        buttons: buttons
                                    })
                                };
                            });
                        }

                        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
                    }
                        .bind(this), 1500 * index
                );

            });

            setTimeout(
                function () {

                    this.setState(prevState => {
                        return {
                            spin: "none"
                        };
                    });

                    this.bottomChild.current.setTextBoxState(false);
                    this.bottomChild.current.inputField.current.focus();
                }
                    .bind(this), 1500 * messageList.length
            );




        }
            


    }

    render() {
        return (
            <div className="chatGlobal">
                <div className="messageAera">
                    <ChatTop />
                    {
                       this.state.items.map
                       (
                           (i, index) => 
                               <BubbleText key={index} message={i.message} type={i.type} buttons={i.buttons} pressButtonChoice={this.pressButtonChoice} /> 
                        )
                    }

                    <div style={{ float: "left", clear: "both" , height:"30px"}}
                        ref={(el) => { this.messagesEnd = el; }}>
                    </div>

                </div>
               

                <ChatBottom ref={this.bottomChild} addMessage={this.addMessage} />
                <Spinner style={{ display: this.state.spin }} animation="grow" />
            </div>
        );
    }
}





export default ChatComponent;