﻿import React from 'react';
import ReactDOM from 'react-dom';
import ChatComponent from './components/ChatComponent';

export default () => (
    ReactDOM.render(
        <div>
            <ChatComponent />
        </div>
        , document.getElementById("root")
    )
);

