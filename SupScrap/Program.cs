﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SupScrap
{
    class Program
    {
        public static int successLoad = 0;

        static async Task Main(string[] args)
        {

            Directory.CreateDirectory("./result");
        
            for(int x = 9402; x>0 && successLoad<500 ; x--)
            {
                await extractPage(x);
            }


            Console.WriteLine("Scrap Finished");
            Console.ReadLine();
        }

        /// <summary>
        /// Get the article data and save it to json file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        async static Task extractPage(int id)
        {
            string url = "https://www.supinfo.com/articles/single/" + id;
            var result = scrapPage(url);

            if(result!=null)
            {
                var json = JsonConvert.SerializeObject(result);
                System.IO.File.WriteAllText($"./result/{id}.json", json);
                Console.WriteLine($"[{successLoad}]Extracted article {id} ==> {result.title}");

                successLoad++;
            }

            await Task.Delay(500);
        }

        /// <summary>
        /// Download the basic page data, title and text
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        static ScrapPage scrapPage(string url)
        {
            Console.WriteLine($"Scraping url: {url}");

            var web = new HtmlWeb();
            var doc = web.Load(url);

            if (doc.Text.Contains("The resource you are looking for has been removed"))
                return null;

            var titleDiv = doc.DocumentNode.Descendants("div").Where(d => d.GetAttributeValue("id", null) == "courseHeader").First();
            string title = titleDiv.ChildNodes.Where(c => c.Name == "h1").First().InnerText;

            var allText = doc.DocumentNode.Descendants("div")
                .Where(d => d.GetAttributeValue("class", null) == "section")
                .Select(sec =>
                   sec.ChildNodes
                   .Where(c => c.Name == "p" && c.InnerText != "\n" && !string.IsNullOrWhiteSpace(c.InnerText))
                   .Select(c => c.InnerText)
                )
                .SelectMany(m => m)
                .ToList();


            string completeText = string.Join("\n", allText);

            var scrapObject = new ScrapPage()
            {
                title = title,
                content = completeText,
                url = url
            };

            return scrapObject;
        }
    }
}
